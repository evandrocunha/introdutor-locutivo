#!/usr/bin/env python3
#encoding: utf-8

import re
import sys
import glob

#definição dos arquivos de entrada e saída no formato "python3 main.py arquivo_de_entrada.txt arquivo_de_saida.txt"
inFile = sys.argv[1]
outFile = sys.argv[2]

entrada = open(inFile, 'r') #abertura do arquivo de entrada definido acima
saida = open(outFile, 'w') #abertura do arquivo de saída definido acima



# Parte 1: cálculo da probabilidade de uma palavra ocorrer em qualquer posição dentro do texto

palavras = []

for linha in entrada:
	for palavra in linha.split():
		palavra_alfa = re.sub(r'[<>\'”“"]', '', palavra)
		if palavra_alfa[0] not in ['\\', '*', '/', '[', '&', '+', '$', '=', ' ']:
			if palavra_alfa not in ['hhh', 'nts', 'yyyy', 'xxx']:
				palavras.append(palavra_alfa.lower())

contagem = {}

for palavra in palavras:
	contagem[palavra] = contagem.get(palavra, 0) + 1

tokens = sum(contagem.values())

print('Parte 1: probabilidade de uma palavra ocorrer em qualquer posição dentro do texto\n', file = saida)

for palavra in contagem:
	print("{}\t{:.5f}".format(palavra, contagem[palavra] / tokens), file = saida)

# Parte 2: cálculo da probabilidade de uma palavra ocorrer num INT no texto

with open(inFile) as arquivo:
	texto = "".join(linha.rstrip() for linha in arquivo)

lista = []

linha_sem_nomes = re.sub(r'\*[A-Z]{3}:', '', texto)
lista.append(re.findall(r'([^=]+) (\S*=\S*=\S*)', linha_sem_nomes, re.DOTALL))

for trecho in lista:
	lista_nova = trecho

palavras = []

for trecho, ato in lista_nova:
	if ato == '/=INT=':
		for palavra in trecho.split():
			palavra_alfa = re.sub(r'[<>\'”“"]', '', palavra)            
			if palavra_alfa[0] not in ['\\', '*', '/', '[', '&', '+', '$', '=', ' ']:
				if palavra_alfa not in ['hhh', 'nts', 'yyyy', 'xxx']:
					palavras.append(palavra_alfa.lower())

contagem = {}

for palavra in palavras:
	contagem[palavra] = contagem.get(palavra, 0) + 1

tokens = sum(contagem.values())

print('\nParte 2: probabilidade de uma palavra ocorrer num INT no texto\n', file = saida)

for palavra in contagem:
	print("{}\t{:.5f}".format(palavra, contagem[palavra] / tokens), file = saida)

print('***Acabou!***')

entrada.close()
saida.close()
